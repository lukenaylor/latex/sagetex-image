FROM docker.io/texlive/texlive:latest

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y wget bzip2 \
    && wget -qO-  https://micromamba.snakepit.net/api/micromamba/linux-64/latest | tar -xvj bin/micromamba \
    && touch /root/.bashrc \
    && ./bin/micromamba shell init -s bash -p /opt/conda  \
    && grep -v '[ -z "\$PS1" ] && return' /root/.bashrc  > /opt/conda/bashrc   # this line has been modified \
    && apt-get clean autoremove --yes \
    && rm -rf /var/lib/{apt,dpkg,cache,log}

SHELL ["bash", "-l" ,"-c"]

RUN source /opt/conda/bashrc && micromamba activate
RUN micromamba shell init --shell=bash --prefix=~/micromamba
RUN echo micromamba activate >> /root/.bashrc
RUN micromamba activate
RUN micromamba install -y -c conda-forge sage

RUN cp -r /root/micromamba/share/texmf/tex/latex/sagetex /usr/local/texlive/2023/texmf-dist/tex/latex/

RUN mktexlsr

RUN mkdir /root/workdir
WORKDIR /root/workdir/
