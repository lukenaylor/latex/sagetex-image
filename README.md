# SageTex Image

Container image with a full texlive, and SageMath installation (who's `sagetex.sty` package is visible to LaTeX).

## Download
Currently ~5-6 GB

```
docker pull registry.gitlab.com/lukenaylor/latex/sagetex-image:latest
```
